#define F_CPU 1000000UL

#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>
#include <util/delay.h>
#include "lcd_lib.h"

#define Kp 0.51
#define Ki 0.078
#define Kd 0.38
#define C_MAX 255
#define C_MIN 0
#define PPR 24
#define setRPM -80

int i = 0;
char result[32];	// ��������� � �������� � ������

float P = 0;
float I = 0;
float D = 0;

float RPM = 0;
float freq = 0;

float limit(float input, float min, float max)
{
	if (input>max) return max;
	if (input<min) return min;
	return input;
}

int PID(float input, float setpoint, float dt)
{
	float error = setpoint - input;
	
	static float prevErr;
	
	P = error;
	I += error * dt;
	D = (error - prevErr) / dt;
	
	prevErr = error;
	
	return P * Kp + I * Ki + D * Kd;
}

void timer0_init()
{
	TCCR0 |= (1 << WGM01) | (1 << WGM00); // FAST MODE PWM
	TCCR0 |= (1 << COM00) | (1 << COM01); // Inverted mode
	TCCR0 |= (1 << CS00);
}

void timer1_init()
{
	TCCR1A = 0x00;
	TCCR1B |= (0b11<<CS10); // ����������� � ������������� ������� 8
	TCNT1H = 0x00;			// �������� ������� ������� ������� ����
	TCNT1L = 0x00;			// �������� ������� ������� ������� ����
	OCR1AH=0b00000010;		// ����������� ������� ��������� ������� ����
	OCR1AL=0b01110001;		// ����������� ������� ��������� ������� ����
	TIMSK |= (1<<OCIE1A);	// ��������� ���������� ������� �� ���������� � OCR1A
}

ISR(TIMER1_COMPA_vect)
{
	RPM = round(freq/PPR*60*25);
	
	freq = 0;
	TCNT1H=0x00;
	TCNT1L=0x00;
}

void isr1_init()
{
	DDRD &= ~(1<<3);		// ����������� ����� �������� ���������� �� ����
	PORTD |= (1<<3);		// �������� ��������
	DDRD &= ~(1<<2);		// ����������� ���� ��� ����������� ������� ������ ��������
	MCUCR &= ~(1<<ISC10);	// ���������� ����� ����������:
	MCUCR |= (1<<ISC11);	// ���������
	GICR |= (1<<INT1);		// ��������� ������� ���������� int1
}

ISR(INT1_vect)
{
	freq++;
}

int main(void)
{
	timer1_init();
	
	isr1_init();
	
	DDRB |= (1 << PB3); // ��� �������
	DDRB |= (1 << PB4); // ������� ���������� ������������
	
	timer0_init();
	
	sei();
	
	lcd_init();
	lcd_clr();
	
	while (1)
	{
		OCR0 = 255-fabs(limit(PID(RPM, fabs(setRPM), 0.01), C_MIN, C_MAX)); // ������ ����������
		PORTB |= setRPM>=0?(1 << PB4):(0<<PB4);								// ������ �����������
	
		dtostrf(RPM, sizeof RPM, 1, result);
		
		lcd_gotoxy(0,0);
		lcd_string(result, 8);
	}
}
