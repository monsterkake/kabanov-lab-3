#ifndef PID_H_
#define PID_H_

#define Kp 0.51
#define Ki 0.078
#define Kd 0.38
#define C_MAX 255
#define C_MIN 0
#define REQUIRED_RPM 100
#define PWM_VOLT_RATIO 0.2046

float P = 0;
float I = 0;
float D = 0;

#include "serial.h"

int limit(int input, int min, int max)
{
	if (input>max) return max;
	if (input<min) return min;
	return input;
}



int PID(float motorSpeed, float requiredSpeed, float dt)
{
	float error = requiredSpeed - motorSpeed;
	int PWM = 0 ;
	
	static float prevErr = 0;
	static int Volt_Out = 0 ;
	
	P = error;
	I += error * dt;
	D = (error - prevErr) / dt;
	
	prevErr = error;
	float PID_value = P * Kp + I * Ki + D * Kd;
	Volt_Out = Volt_Out + PID_value ;

	PWM = Volt_Out * PWM_VOLT_RATIO;

	return PWM;
}




#endif /* PID_H_ */