

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define FOSC 8000000UL
#define BAUD 2400
#define MYUBRR FOSC/16/BAUD-1

const int PIND_MASK = 0b00001100;

const int sampleTime = 10; // msec

#include "serial.h"
#include "pid.h"

void timer1_init()
{
	// https://alselectro.wordpress.com/2017/08/15/avr-self-learning-timer1-interrupts/
	
	const int prescaler = 256; // CS12 is a 256 prescaler.

	const int clock_frequency = FOSC / prescaler;
	const float tickLength = 1.0 / (float)clock_frequency * 1000.0; // Time period is reciprocal of freq
	const int countsToGenerateDelay =  (float)sampleTime / tickLength;
	
	TCCR1B |= (1 << WGM12) | (1 << CS12); // Enable Clear Timer on Compare mode, and set prescaler to CS12(256).
	TCNT1 = 0;	// Start with 0.
	OCR1A = countsToGenerateDelay; // To generate delay of "sampleTime", you need "countsToGenerateDelay" counts.
	TIMSK |= (1 << OCIE1A);
}

void timer0_init()
{
	TCCR0 |= (1 << WGM01) | (1 << WGM00); // FAST MODE PWM
	TCCR0 |= (1 << COM00) | (1 << COM01); // Inverted mode
	TCCR0 |= (1 << CS00);
}

void USART_Init(unsigned int ubrr)
{
	UBRRH = (unsigned char)(ubrr>>8);
	UBRRL = (unsigned char)ubrr;
	UCSRB = (1<<RXEN)|(1<<TXEN)|(1<<RXCIE);
	UCSRC = (1<<USBS)|(3<<UCSZ0);
}

int totalTimerInterrupts = 0;
int miliSecondsPassed = 0;

ISR (TIMER1_COMPA_vect)
{
	totalTimerInterrupts++;
	miliSecondsPassed = totalTimerInterrupts * sampleTime;
}

int impulseCounter = 0;
float speed_RPM = 0;
ISR(INT0_vect)
{
	const int pulsesPerOneRevolution = 300;
	impulseCounter+=1;
	if(impulseCounter > pulsesPerOneRevolution){
		impulseCounter = 0;
		speed_RPM = 1.0 / miliSecondsPassed * 1000.0 * 60.0;
		totalTimerInterrupts = 0;

		sendFloat(speed_RPM);
		sendEndl();
		
		float dt = miliSecondsPassed / 1000.0;
		//OCR0 = PID(speed_RPM, fabs(REQUIRED_RPM), dt);
		PORTB |= REQUIRED_RPM>=0?(0b00010000):(0b00000000);
		OCR0 = 255; // 255 = 11 volts 
		//OCR0 = 0;
	}
}





int main(void)
{
	cli();
	USART_Init(MYUBRR);
	timer1_init();
	timer0_init();

	_delay_ms(100);
	
	DDRB &= ~(1<<2);
	PORTB |= (1<<2);
	DDRB &= ~(1<<1);
	DDRB |= (1<<0);
	MCUCR &= ~(1<<ISC2);
	sei();
	GICR |= (1<<INT0);
	while(1)
	{
	}
}