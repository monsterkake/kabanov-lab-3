#ifndef SERIAL_H_
#define SERIAL_H_

#include <avr/io.h>
#include <stdlib.h>

void sendString(char *string);
void sendEndl();
void sendFloat(float value);

#endif /* SERIAL_H_ */