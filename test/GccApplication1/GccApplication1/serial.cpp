#include "serial.h"

void USART_Transmit(unsigned int data)
{
	while ( !(UCSRA & (1<<UDRE)));
	UCSRB &= ~(1<<TXB8);
	if (data & 0x0100) UCSRB |= (1<<TXB8);
	UDR = data;
}

void sendString(char *string)
{
	while(*string !='\0')
	{
		USART_Transmit(*string);
		string++;
	}
}
void sendEndl(){
	sendString("\r\n");
}

void sendFloat(float value){
	char tempStr[32];
	dtostrf(value, sizeof value, 1, tempStr);
	sendString(tempStr);
}